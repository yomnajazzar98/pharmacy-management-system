<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user=User::create([
            'name' => 'app manager',
            'email' => 'app_manager@gmail.com',
            'password'=> Hash::make('app_manager'),
            'name_role'=>'admin',

        ]);
        $user->attachRole(2);
    }
}
