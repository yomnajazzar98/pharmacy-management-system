<?php

//namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PharmacyBranch\Branch;

class BranchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory(App\Models\PharmacyBranch\Location::class, 3)->create();
        Branch::create([
            'email' => 'main_branch@gmail.com',
            'name' => 'main branch',
            'location_id'=>1,
            'type'=>1
        ]);
        factory(App\Models\PharmacyBranch\Branch::class, 1)->create();
        // factory(App\User::class, 2)->create();
    }
}
