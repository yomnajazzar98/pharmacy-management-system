<?php

//namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $this->call(RoleSeeder::class);
      $this->call(BranchTableSeeder::class);
      $this->call(UserTableSeeder::class);

      $this->call(ProductTableSeeder::class);
       $this->call(OrderTableSeeder::class);



    }
}
 