<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Warehouse extends Model
{
    use SoftDeletes;

    protected $table = 'warehouses';

    // The attributes that are mass assignable.
    protected $fillable = [
        'name', 'address', 'mobile', 'email',
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    ##############################Relationships##############################

    /*public function buyOrders()
    {
    	return $this->hasMany('App\Models\BuyOrder', 'ware_house_id', 'id');
    }*/
}
