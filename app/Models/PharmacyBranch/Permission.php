<?php

namespace  App\Models\PharmacyBranch;

use Laratrust\Models\LaratrustPermission;

class Permission extends LaratrustPermission
{
    public $guarded = [];
    
}
