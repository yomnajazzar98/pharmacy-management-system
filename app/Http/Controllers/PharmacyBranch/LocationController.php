<?php

namespace App\Http\Controllers\PharmacyBranch;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PharmacyBranch\Location;
use App\Traits\UserJobTrait;

class LocationController extends Controller
{
        use UserJobTrait;

     public function add()
   {
     $locations=Location::all();

    return view('dashboard.Location.add',compact('locations'));
   }

   public function store(Request $request)
   {
  
        $location =new Location;
        $location->name=$request->name;
        $location->parent_id=$request->parent_id;
        $location->save();
        $title="this user add location where id  ".$location->id;
           $this->userJob($title);
        return redirect('/locations/all');
   }

   public function all()
   {
    $locations=Location::all();
    return view('dashboard.Location.all',compact('locations'));
   } 


   public function delete($id)
   {
     $locations=Location::find('id',$id);
          $title="this user delete location where id  ".$locations->id;
           $this->userJob($title);
     $locations->delete();
     return back();

   }


   public function edit($id)
   {
    $location=Location::find($id);
    $locations=Location::all();
   
    return view('dashboard.Location.edit',compact('locations','location'));
   }


      public function update($id,Request $request)
   {
          //return $id;
         // return $request->id;
        $locations=Location::find($id);
        $locations->name=$request->name;
        $locations->parent_id=$request->parent_id;
        $locations->save();
           $title="this user update location where id  ".$locations->id;
           $this->userJob($title);
        return back();
   }
}

