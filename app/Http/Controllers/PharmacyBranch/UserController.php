<?php
namespace App\Http\Controllers\PharmacyBranch;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\PharmacyBranch\Branch;
use Carbon\Carbon;
use App\Role;
use App\Models\PharmacyBranch\Permission;
use App\Traits\UserJobTrait;
use App\Models\PharmacyBranch\UserJob;

class UserController extends Controller
{
        use UserJobTrait;


   public function all()
   {
    $users=User::all();
    return view('PharmacyBranch.User.user_list',compact('users'));
   } 

   public function delete($id)
   {

     $user=User::where('id',$id);
         $title="this user delete user where id  ".$user->id;
           $this->userJob($title);
     $user->delete();
     return back();

   }


 public function edit($id)
   {
   $user=User::find($id);
    $roles=Role::all();
    return view('PharmacyBranch.User.edit_user',compact('user','roles'));
   }
public function update(Request $request,$id)
{
    $user=User::find($id);
    $title="this user update user where id  ".$user->id;
    $this->userJob($title);

    $user->salary=$request->salary;
    $user->working_hours=$request->working_hours;

    if($request->role) 
    {
    $user->detachRoles($user->getRoles());
    $user->attachRole($request->role);
    $user->name_role=$request->role;
    }

      $user->save();
   

      if($user->hasRole('employeeInventory'))
      {
        $branches=Branch::where('type',0)->get();
      }else if($user->hasRole('employeePharmacy'))
       {
        $branches=Branch::where('type',1)->get();
      }else {
        $branches=Branch::all();
      }

    return view('PharmacyBranch.User.select_Branch',compact('user','branches'));

}
public function selectBranch($id,Request $request)
{
       $user=User::find($id);
      $user->branch_id=$request->branch;
      $user->save();
     return redirect('/User/all');



}





        public function info_user()
        {
        return view('PharmacyBranch.User.my_profile');
        }

        public function edit_user()
        { 

        return view('PharmacyBranch.User.edit_profile');


        }
         public function update_user(Request $request)
        {

          	if(auth()->guard('web')->user()->id>0)
          	{
          $user=User::find(auth()->guard('web')->user()->id);
          $user->name=$request->name;
          $user->email=$request->email;
          $user->mobile=$request->mobile;

          $user->save();    
        return back();
        }
    


}

    public function userActive()
        {
          
          $userjobs=UserJob::all();
         return view('PharmacyBranch.User.user_activity',compact('userjobs')); 

        }


}