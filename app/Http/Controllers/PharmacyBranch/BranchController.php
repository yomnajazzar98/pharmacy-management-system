<?php
namespace App\Http\Controllers\PharmacyBranch;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PharmacyBranch\Branch;
use App\Models\PharmacyBranch\Location;
use App\Traits\UserJobTrait;

class BranchController extends Controller
{
      use UserJobTrait;

   public function store(Request $request)
   {
    //return $request;
        $branch =new Branch;
        $branch->name=$request->name;
        $branch->email=$request->email;
        $branch->location_id=$request->Location;
        $branch->type=$request->type;
        $branch->active=$request->active;
        $branch->save();
           $title="this user add branch".$request->name."where id  ".$branch->id;
           $this->userJob($title);
        return redirect('/branchs/all');

   }

      public function add()
   {
   $locations=Location::all();
    return view('PharmacyBranch.Branch.add_branch',compact('locations'));
   }


   public function all()
   {
    $branchs=Branch::all();
    return view('PharmacyBranch.Branch.branch_list',compact('branchs'));
   } 


   public function delete($id)
   {
     $branchs=Branch::find($id);
      $title="this user delete branch".$branchs->name."where id  ".$branchs->id;
      $this->userJob($title);
     $branchs->delete();

     return back();

   }


   public function edit($id)
   {
    $branch=Branch::find($id);
   $locations=Location::all();

    return view('PharmacyBranch.Branch.branch_edit',compact('branch','locations'));
   }


      public function update($id,Request $request)
   {
        $branch=Branch::find($id);
       $branch->name=$request->name;
        $branch->email=$request->email;
        $branch->location_id=$request->Location;
        $branch->type=$request->type;
        $branch->active=$request->active;
        $branch->save();
         $title="this user update branch".$branch->name."where id  ".$branch->id;
           $this->userJob($title);
        return redirect('/branchs/all');
   }





}

  // public function activity()
  //       {
  //           return $this->hasMany('App\Activity')
  //               ->with(['user', 'subject'])
  //               ->latest()->get();
  //       }