<?php

return [


    'previous' => '&laquo; Previous',
    'next' => 'Next &raquo;',


    // Dash Board
    'pharmacy' => 'صيدلية',
    'user_name' => 'اسم المستخدم',
    'home' => 'الرئيسية',
    'customer' => 'الزبائن',
    'manufacturer' => 'المصنع',
    'medicine' => 'الأدوية',
    'food' => 'الأغذية الطبية',
    'cosmetic' => 'مستحضرات التجميل',
    'supply' => 'مستلزمات طبية',
    'purchase' => 'المشتريات',
    'invoice' => 'الفواتير',
    'return' => 'المرجعات',
    'add_customer' => 'إضافة زبون',
    'customer_list' => 'قائمة الزبائن',
    'add_manufacturer' => 'إضاغة مصنع',
    'manufacturer_list' => 'قائمة المصانع',
    'add_category' => 'إضافة صنف',
    'category_list' => 'قائمة الأصناف',
    'add_type' => 'إضافة نوع',
    'type_list' => 'قائمة الأنواع',
    'add_age_group' => 'إضافة فئة عمرية',
    'age_group_list' => 'قائمة الفئات العمرية',
    'age_group' => 'الفئة العمرية',
    'add_effective_material' => 'إضافة مادة فعالة',
    'effective_material_list' => 'قائمة المواد الفعالة',
    'add_medicine' => 'إضافة دواء',
    'medicine_list' => 'قائمة الأدوية',
    'add_food' => 'إضافة غذاء طبي',
    'food_list' => 'قائمة الأغذية الطبية',
    'add_cosmetic' => 'إضافة مستحضر تجميل',
    'cosmetic_list' => 'قائمة مستحضرات التجميل',
    'add_supply' => 'إضافة مستلزم طبي',
    'supply_list' => 'قائمة المستلزمات الطبية',
    'add_order' => 'إضافة طلب',
    'purchase_list' => 'قائمة المشتريات',
    'add_invoice' => 'إضافة فاتورة',
    'invoice_list' => 'قائمة الفواتير',
    'add_return' => 'إضافة فاتورة ترجيع من زبون',
    'return_list' => 'قائمة فواتير الإرجاع',
    'add_manu_return' => 'إضافة فاتورة ترجيع من مصنع',
    'manu_return_list' => 'قائمة فواتير الإرجاع من مصنع',


    //Body Page
    'grid' => 'عرض شبكي',
    'search' => 'البحث',

    'id' => 'الرقم',
    'medicine_name' => 'الاسم العلمي',
    'generic_name' => 'الاسم التجاري',
    'category' => 'الصنف',
    'type' => 'النوع',
    'agegroup' => 'الفئة العمرية',
    'effective_material' => 'المواد الفعالة',
    'strength' => 'العيار',
    'composition' => 'التركيب',
    'alternatives' => 'الأدوية البديلة',
    'Indications' => 'الاستطبابات',
    'product_country' => 'البلد المصدر',
    'warehouse' => 'المستودع',
    'purchasing_price' => 'سعر الشراء',
    'selling_price' => 'سعر المبيع',
    'production_date' => 'تاريخ الصنع',
    'expired_date' => 'تاريخ انتهاء الصلاحية',
    'bar_code' => 'الباركود',
    'prescription' => 'الحاجة لوصفة طبية',
    'image' => 'الصورة',
    'action' => 'القيام ب',

    'this_field_is_required' => 'هذاالحقل مطلوب.',
    'add' => 'إضافة',
    'edit_type' => 'تعديل النوع',
    'type_name' => 'اسم النوع',

    'edit_age_group' =>'تعديل الفئة العمرية',
    'edit' =>'تعديل',
    'edit_category' =>'تعديل صنف الدواء',

    'category_name' =>'اسم الصنف',

    'name' =>'الاسم',

    'ingredients' =>'المكونات',
    'description' =>'الوصف',

    'choose_file' =>'اختر صورة',

    'cosmetic_product_grid' =>'شبكة مستحضرات التجميل',
    'list_view' =>'عرض القائمة',

    'show_details' =>'عرض التفاصيل',

    'quantity' =>'الكمية',
    'edit_cosmetics' =>'تعديل المستحضر',
    'no_file_chosen' =>'لم يتم اختيار ملف',

    'display_cosmetic_product_batches' =>'عرض دفعات مستحضر التجميل',

    'amount' =>'الكمية',
    'closet' =>'الخزانة',
    'rack' =>'الرف',
    'drawer' =>'درج',

    'ware_house' => 'المخزن',



    'details_cosmetic-product'=>'تفاصيل مستحضرالتجميل',
    'storage'=>'التخزين',
    'adverse_effects'=>'التأثيرات الجانبية',
    'edit_medical_food'=>'تعديل الغذاء الطبي',
    'medical_food_grid'=>'شبكة الأغذية الطبية',
    ' display_food_batches'=>'عرض دفعات الطعام',
    'Batch_Id'=>'رقم الدفعة',
    'Medical_Food_Name'=>'اسم الغذاء الطبي',
    'Details_Medical_Food'=>'تفاصيل الغذاء الطبي',


    'Setting'=>'إعدادات التطبيق',
    'Application'=>'التطبيق',
    'Warehouse_Orders_Report'=>'تقارير طلبيات المستودعات',
    'Warehouses_Report'=>'تقارير المستودعات',
    'Return_Invoices_Report'=>' تقارير فواتير الإرجاع',
    'Invoices_Report'=>'تقارير الفولتير',

    'Report'=>'تقارير',
    'Dashboard'=>'الصفحة الرئيسية',
    'User_Activity'=>'الموظفين النشطاء',
    'User_List'=>'قائمة الموظفين',
    'User'=>'الموظف',
    'Products_Report'=>'تقارير المنتجات',

    'Medical_Supply'=>'المستلزمات الطبية',


    'medical_food'=>'الأغذية الطبية',

    'Cosmetics'=>'مستحضرات التجميل',
    'price'=>'السعر',
    'batch'=>'الدفعة',
    'batch_date' =>'رقم الدفعة',

    'details_cosmetic'=>'تفاصيل مستحضر التجميل',

    'cosmetic_product_name'=>'اسم مستحضر التجميل',

];
